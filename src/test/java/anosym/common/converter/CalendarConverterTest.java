package anosym.common.converter;

import java.util.Calendar;
import java.util.TimeZone;

import org.junit.jupiter.api.Test;

import anosym.common.converter.CalendarConverter;
import anosym.common.converter.ConverterException;

import static anosym.common.converter.CalendarConverter.toTimeZone;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author marembo
 */
public class CalendarConverterTest {

  private static final String FORMAT = "yyyy-MM-dd HH:mm:ss";

  private static final CalendarConverter CALENDAR_CONVERTER = new CalendarConverter();

  private static final long NOW_MILLIS = 1399834894443L;

  private static final String NOW_EXPECTED = "2014-05-11 21:01:34";

  private static final String EAT_EXPECTED = "2014-05-12 00:01:34";

  private static final String EAT_EXPECTED_FROM_NEW_YORK = "2014-05-12 05:01:34";

  @Test
  public void testConvertFrom() {
    final String format = "yyyy-MM-dd HH:mm";
    final String value = "2014-03-23 12:00";
    final Calendar expected = Calendar.getInstance();
    expected.setTimeInMillis(0);
    //months start from zero
    expected.set(2014, 02, 23, 12, 0);
    final Calendar actual = CALENDAR_CONVERTER.from(value, format);
    assertThat(expected, is(actual));
  }

  @Test
  public void testConvertFromWithSeveralFormats() {
    final String formats[] = {"yyyy-MM-dd HH:mm", "yyyy-MM-dd'T'HH:mm:ss'Z'"};
    final String value = "2014-03-23T12:00:00Z";
    final Calendar expected = Calendar.getInstance();
    expected.setTimeInMillis(0);
    //months start from zero
    expected.set(2014, 02, 23, 12, 0);
    final Calendar actual = CALENDAR_CONVERTER.from(value, formats);
    assertThat(expected, is(actual));
  }

  @Test
  public void testConvertFromWithSeveralFormatsWithInvalidDate() {
    final String formats[] = {"yyyy-MM-dd HH:mm", "yyyy-MM-dd'T'HH:mm:ss'Z'"};
    final String value = "2014-03-23T12:00:00.000Z";
    final Calendar expected = Calendar.getInstance();
    expected.setTimeInMillis(0);
    //months start from zero
    expected.set(2014, 02, 23, 12, 0);
    assertThrows(ConverterException.class, () -> CALENDAR_CONVERTER.from(value, formats));
  }

  @Test
  public void testConvertTo() {
    final String format = "yyyy-MM-dd HH:mm";
    final String value = "2014-03-23 12:00";
    final Calendar expected = Calendar.getInstance();
    expected.setTimeInMillis(0);
    //months start from zero
    expected.set(2014, 02, 23, 12, 0);
    final String actual = CALENDAR_CONVERTER.to(expected, format);
    assertThat(value, is(actual));
  }

  @Test
  public void testConvertToWithSeveralFormats() {
    final String formats[] = {"Invalid Format", "yyyy-MM-dd'T'HH:mm:ss'Z'"};
    final String value = "2014-03-23T12:00:00Z";
    final Calendar expected = Calendar.getInstance();
    expected.setTimeInMillis(0);
    //months start from zero
    expected.set(2014, 02, 23, 12, 0);
    final String actual = CALENDAR_CONVERTER.to(expected, formats);
    assertThat(actual, is(value));
  }

  @Test
  public void verifyToTimeZone() {
    final Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    now.setTimeInMillis(NOW_MILLIS);

    final TimeZone eatTimeZone = TimeZone.getTimeZone("Africa/Nairobi");
    final Calendar withEatTimeZone = toTimeZone(now, eatTimeZone);
    final String utcTimeStr = CALENDAR_CONVERTER.to(now, FORMAT);
    final String eatTimeStr = CALENDAR_CONVERTER.to(withEatTimeZone, FORMAT);
    assertThat(utcTimeStr, is(NOW_EXPECTED));
    assertThat(eatTimeStr, is(EAT_EXPECTED));
  }

  @Test
  public void verifyToTimeZoneFromNonUtc() {
    final Calendar now = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
    now.setTimeInMillis(NOW_MILLIS);

    final TimeZone eatTimeZone = TimeZone.getTimeZone("Africa/Nairobi");
    final Calendar withEatTimeZone = toTimeZone(now, eatTimeZone);
    final String utcTimeStr = CALENDAR_CONVERTER.to(now, FORMAT);
    final String eatTimeStr = CALENDAR_CONVERTER.to(withEatTimeZone, FORMAT);
    assertThat(utcTimeStr, is(NOW_EXPECTED));
    assertThat(eatTimeStr, is(EAT_EXPECTED_FROM_NEW_YORK));
  }

}
