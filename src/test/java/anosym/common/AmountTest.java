package anosym.common;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import anosym.common.Amount;
import anosym.common.Currency;

import static anosym.common.Amount.GLOBAL_CENT_ROUNDING_MODE_PROPERTY;
import static anosym.common.Amount.CentRoundingMode.NEAREST_FIFTY_CENTS;
import static anosym.common.Amount.CentRoundingMode.NEAREST_FIVE_CENTS;
import static anosym.common.Amount.CentRoundingMode.NEAREST_HUNDRED_CENTS;
import static anosym.common.Amount.CentRoundingMode.NEAREST_TEN_CENTS;
import static anosym.common.Amount.CentRoundingMode.SPECIFIED_ACCURACY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;

/**
 *
 * @author marembo
 */
public class AmountTest {

  @BeforeEach
  public void tearDown() {
    System.clearProperty(GLOBAL_CENT_ROUNDING_MODE_PROPERTY);
  }

  @Test
  public void testNearestCent() {
    final Amount amount = new Amount(Currency.AED, 233444);
    assertThat(amount.getValueInCents(), is(233444l));
  }

  @Test
  public void testNearestFiftyCent() {
    final Amount amount = new Amount(Currency.AED, 233444, NEAREST_FIFTY_CENTS);
    assertThat(amount.getValueInCents(), is(233450l));
  }

  @Test
  public void testNearestFiftyCentToHundrend() {
    final Amount amount = new Amount(Currency.AED, 233468, NEAREST_FIFTY_CENTS);
    assertThat(amount.getValueInCents(), is(233500l));
  }

  @Test
  public void testNearestFiveCent() {
    final Amount amount = new Amount(Currency.AED, 233444, NEAREST_FIVE_CENTS);
    assertThat(amount.getValueInCents(), is(233445l));
  }

  @Test
  public void testNearestFiveCentToTen() {
    final Amount amount = new Amount(Currency.AED, 233448, NEAREST_FIVE_CENTS);
    assertThat(amount.getValueInCents(), is(233450l));
  }

  @Test
  public void testNearestHundrenCent() {
    final Amount amount = new Amount(Currency.AED, 233444, NEAREST_HUNDRED_CENTS);
    assertThat(amount.getValueInCents(), is(233400l));
  }

  @Test
  public void testNearestHundrenCentToHundrend() {
    final Amount amount = new Amount(Currency.AED, 233468, NEAREST_HUNDRED_CENTS);
    assertThat(amount.getValueInCents(), is(233500l));
  }

  @Test
  public void testNearestTenCent() {
    final Amount amount = new Amount(Currency.AED, 233444, NEAREST_TEN_CENTS);
    assertThat(amount.getValueInCents(), is(233440l));
  }

  @Test
  public void testNearestTenCentToTen() {
    final Amount amount = new Amount(Currency.AED, 233448, NEAREST_TEN_CENTS);
    assertThat(amount.getValueInCents(), is(233450l));
  }

  @Test
  public void testNearestCentDouble() {
    final Amount amount = new Amount(Currency.AED, 2334.44);
    assertThat(amount.getValueInCents(), is(233444l));
  }

  @Test
  public void testNearestFiftyCentDouble() {
    final Amount amount = new Amount(Currency.AED, 2334.44, NEAREST_FIFTY_CENTS);
    assertThat(amount.getValueInCents(), is(233450l));
  }

  @Test
  public void testNearestFiftyCentToHundrendDouble() {
    final Amount amount = new Amount(Currency.AED, 2334.68, NEAREST_FIFTY_CENTS);
    assertThat(amount.getValueInCents(), is(233500l));
  }

  @Test
  public void testNearestFiveCentDouble() {
    final Amount amount = new Amount(Currency.AED, 2334.44, NEAREST_FIVE_CENTS);
    assertThat(amount.getValueInCents(), is(233445l));
  }

  @Test
  public void testNearestFiveCentToTenDouble() {
    final Amount amount = new Amount(Currency.AED, 2334.48, NEAREST_FIVE_CENTS);
    assertThat(amount.getValueInCents(), is(233450l));
  }

  @Test
  public void testNearestHundrenCentDouble() {
    final Amount amount = new Amount(Currency.AED, 2334.44, NEAREST_HUNDRED_CENTS);
    assertThat(amount.getValueInCents(), is(233400l));
  }

  @Test
  public void testNearestHundrenCentToHundrendDouble() {
    final Amount amount = new Amount(Currency.AED, 2334.68, NEAREST_HUNDRED_CENTS);
    assertThat(amount.getValueInCents(), is(233500l));
  }

  @Test
  public void testNearestTenCentDouble() {
    final Amount amount = new Amount(Currency.AED, 2334.44, NEAREST_TEN_CENTS);
    assertThat(amount.getValueInCents(), is(233440l));
  }

  @Test
  public void testNearestTenCentToTenDouble() {
    final Amount amount = new Amount(Currency.AED, 2334.48, NEAREST_TEN_CENTS);
    assertThat(amount.getValueInCents(), is(233450l));
  }

  @Test
  public void testNearestTenCentToTenDoubleFromGlobalCentRoundingMode() {
    System.setProperty(GLOBAL_CENT_ROUNDING_MODE_PROPERTY, "NEAREST_TEN_CENTS");
    final Amount amount = new Amount(Currency.AED, 2334.48);
    assertThat(amount.getValueInCents(), is(233450l));
  }

  @Test
  public void testNearestFiftyCentToTenDoubleFromGlobalCentRoundingMode() {
    System.setProperty(GLOBAL_CENT_ROUNDING_MODE_PROPERTY, "NEAREST_FIFTY_CENTS");
    final Amount amount = new Amount(Currency.AED, 2334.20);
    assertThat(amount.getValueInCents(), is(233450l));
  }

  @Test
  public void testToString() {
    final Amount amount = new Amount(Currency.KES, 233448, NEAREST_TEN_CENTS);
    assertThat(amount.toString(), is(equalTo("KES 2,334.50")));
  }

  @Test
  public void testToStringNearestFiveCents() {
    final Amount amount = new Amount(Currency.KES, 233403, NEAREST_FIVE_CENTS);
    assertThat(amount.toString(), is(equalTo("KES 2,334.05")));
  }

  @Test
  public void testMultiplyByDouble() {
    final Amount amount = new Amount(Currency.KES, 450080);
    final Amount expected = new Amount(Currency.KES, 153027);
    final Amount result = amount.multiply(0.34);
    assertThat(result, is(expected));
  }

  @Test
  public void testSpecifiedAccuracy() {
    final Amount amount = new Amount(Currency.KES, 553.566224, SPECIFIED_ACCURACY, 4);
    final String expected = "KES 553.5662";
    assertThat(amount.toString(), is(expected));
  }

  @Test
  public void testSpecifiedAccuracyRounded() {
    final Amount amount = new Amount(Currency.KES, 553.5662824, SPECIFIED_ACCURACY, 4);
    final String expected = "KES 553.5663";
    assertThat(amount.toString(), is(expected));
  }

  @Test
  public void testAddAmountsWithDifferentAccuracyScales() {
    final Amount amount1 = new Amount(Currency.USD, 20000, SPECIFIED_ACCURACY, 4);
    final Amount amount2 = new Amount(Currency.USD, 20000, SPECIFIED_ACCURACY, 3);
    final Amount expected = new Amount(Currency.USD, 220000, SPECIFIED_ACCURACY, 4);
    final Amount actual = amount1.add(amount2);
    assertThat(actual, is(expected));
  }

  @Test
  public void testAddAmountsWithDifferentCentRoundingMode() {
    final Amount amount1 = new Amount(Currency.USD, 20000, NEAREST_TEN_CENTS, 2);
    final Amount amount2 = new Amount(Currency.USD, 20000, SPECIFIED_ACCURACY, 3);
    final Amount expected = new Amount(Currency.USD, 220000, SPECIFIED_ACCURACY, 3);
    final Amount actual = amount1.add(amount2);
    assertThat(actual, is(expected));
  }

  @Test
  public void testSubtractAmountsWithDifferentAccuracyScales() {
    final Amount amount1 = new Amount(Currency.USD, 20000, SPECIFIED_ACCURACY, 4);
    final Amount amount2 = new Amount(Currency.USD, 20000, SPECIFIED_ACCURACY, 3);
    final Amount expected = new Amount(Currency.USD, 180000, SPECIFIED_ACCURACY, 4);
    final Amount actual = amount2.subtract(amount1);
    assertThat(actual, is(expected));
  }

  @Test
  public void testCompareAmountsWithDifferentAccuracyScalesLessThan() {
    final Amount amount1 = new Amount(Currency.USD, 20000, SPECIFIED_ACCURACY, 4);
    final Amount amount2 = new Amount(Currency.USD, 20000, SPECIFIED_ACCURACY, 3);
    final int comparison = amount1.compareTo(amount2);
    assertThat(comparison, is(lessThan(0)));
    assertThat(amount1.equals(amount2), is(false));
  }

  @Test
  public void testCompareAmountsWithDifferentAccuracyScalesEquals() {
    final Amount amount1 = new Amount(Currency.USD, 200000, SPECIFIED_ACCURACY, 4);
    final Amount amount2 = new Amount(Currency.USD, 20000, SPECIFIED_ACCURACY, 3);
    final int comparison = amount1.compareTo(amount2);
    assertThat(comparison, is(equalTo(0)));
    assertThat(amount1.equals(amount2), is(true));
  }

  @Test
  public void testCompareAmountsWithDifferentAccuracyScalesGreaterThan() {
    final Amount amount1 = new Amount(Currency.USD, 20000, SPECIFIED_ACCURACY, 4);
    final Amount amount2 = new Amount(Currency.USD, 20000, SPECIFIED_ACCURACY, 3);
    final int comparison = amount2.compareTo(amount1);
    assertThat(comparison, is(greaterThan(0)));
    assertThat(amount1.equals(amount2), is(false));
  }

  @Test
  public void testBigDecimalWithZeroScale() {
    final Amount amount1 = new Amount(Currency.USD, BigDecimal.valueOf(2));
    assertThat(amount1.getAccuracyScale(), is(2));
  }

  @Test
  public void testUpScaling() {
    final Amount amount = new Amount(Currency.USD, new BigDecimal("20.55"));
    final Amount scaledAmount = amount.scale(4);
    final Amount expected = new Amount(Currency.USD, new BigDecimal("20.5500"));
    assertThat(amount.toString(), is(not(equalTo(expected.toString()))));
    assertThat(scaledAmount.toString(), is(expected.toString()));
  }

  @Test
  public void testDownScaling() {
    final Amount amount = new Amount(Currency.USD, new BigDecimal("20.55000"));
    final Amount scaledAmount = amount.scale(2);
    final Amount expected = new Amount(Currency.USD, new BigDecimal("20.55"));
    assertThat(amount.toString(), is(not(equalTo(expected.toString()))));
    assertThat(scaledAmount.toString(), is(expected.toString()));
  }

  @Test
  public void test1018AsDecimal() {
    final Amount amount = new Amount(Currency.USD, 1018.00);
    final Amount expected = new Amount(Currency.USD, new BigDecimal("1018.00"));
    assertThat(amount.toString(), is(expected.toString()));
  }

  @Test
  public void test1018AsIntegral() {
    final Amount amount = new Amount(Currency.USD, 1018);
    final Amount expected = new Amount(Currency.USD, new BigDecimal("10.18"));
    assertThat(amount.toString(), is(expected.toString()));
  }

}
