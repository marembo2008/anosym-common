package anosym.common.validation;

import java.math.BigDecimal;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import jakarta.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import anosym.common.validation.NonnullPropertiesValidator;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Dec 21, 2016, 1:30:26 PM
 */
public class NonnullPropertiesValidatorTest {

  private final ConstraintValidatorContext constraintValidatorContext = mock(ConstraintValidatorContext.class);

  private final NonnullPropertiesValidator nonnullPropertiesValidator = new NonnullPropertiesValidator();

  @BeforeEach
  public void setUp() {
    final ConstraintValidatorContext.ConstraintViolationBuilder constraintViolationBuilder
            = mock(ConstraintValidatorContext.ConstraintViolationBuilder.class);
    when(constraintValidatorContext.buildConstraintViolationWithTemplate(anyString())).thenReturn(
            constraintViolationBuilder);
  }

  @Test
  public void verifyNonNullProperties() {
    final TestNonnullProperty testNonnullProperty = new TestNonnullProperty("value", 78, null);
    assertThat(nonnullPropertiesValidator.isValid(testNonnullProperty, constraintValidatorContext), is(true));
  }

  @Test
  public void verifyNonNullPropertiesNotSet() {
    final TestNonnullProperty testNonnullProperty = new TestNonnullProperty("value", null, null);
    assertThat(nonnullPropertiesValidator.isValid(testNonnullProperty, constraintValidatorContext), is(false));
  }

  @Test
  public void verifyNullableProperties() {
    final TestNullableProperty testNullableProperty = new TestNullableProperty(null, null, null);
    assertThat(nonnullPropertiesValidator.isValid(testNullableProperty, constraintValidatorContext), is(true));
  }

  private static final class TestNonnullProperty {

    @Nonnull
    private String value;

    @Nonnull
    private Integer index;

    @Nullable
    private BigDecimal amount;

    public TestNonnullProperty(String value, Integer index, BigDecimal amount) {
      this.value = value;
      this.index = index;
      this.amount = amount;
    }

  }

  private static final class TestNullableProperty {

    private String value;

    @Nullable
    private Integer index;

    @Nullable
    private BigDecimal amount;

    public TestNullableProperty(String value, Integer index, BigDecimal amount) {
      this.value = value;
      this.index = index;
      this.amount = amount;
    }

  }

}
