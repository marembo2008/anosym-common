package anosym.common.json;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import anosym.common.jpa.LocalDateConverter;
import anosym.common.jpa.LocalDateTimeConverter;
import anosym.common.jpa.LocalTimeConverter;
import anosym.common.jpa.OffsetDateTimeConverter;
import anosym.common.jpa.OffsetTimeConverter;
import anosym.common.jpa.ZonedDateTimeConverter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 15, 2017, 9:39:57 PM
 */
public class Jdk8TimeAttributeConverterTest {

  @Test
  public void testLocalDateTime() {
    final LocalDateTimeConverter c = new LocalDateTimeConverter();
    final LocalDateTime ldt = LocalDateTime.of(2010, 10, 12, 12, 30, 33);
    final String dbAttr = c.convertToDatabaseColumn(ldt);
    assertThat(dbAttr, is("2010-10-12 12:30:33"));
  }

  @Test
  public void testLocalDate() {
    final LocalDateConverter c = new LocalDateConverter();
    final LocalDate ldt = LocalDate.of(2010, 10, 12);
    final String dbAttr = c.convertToDatabaseColumn(ldt);
    assertThat(dbAttr, is("2010-10-12"));
  }

  @Test
  public void testLocalTime() {
    final LocalTimeConverter c = new LocalTimeConverter();
    final LocalTime ldt = LocalTime.of(12, 30, 12);
    final String dbAttr = c.convertToDatabaseColumn(ldt);
    assertThat(dbAttr, is("12:30:12"));
  }

  @Test
  public void testOffsetTime() {
    final OffsetTimeConverter c = new OffsetTimeConverter();
    final LocalTime time = LocalTime.of(12, 30, 12);
    final OffsetTime ldt = OffsetTime.of(time, ZoneOffset.ofHours(6));
    final String dbAttr = c.convertToDatabaseColumn(ldt);
    assertThat(dbAttr, is("12:30:12+06:00"));
  }

  @Test
  public void testOffsetDateTime() {
    final OffsetDateTimeConverter c = new OffsetDateTimeConverter();
    final LocalDateTime dateTime = LocalDateTime.of(2010, 10, 12, 12, 30, 33);
    final OffsetDateTime ldt = OffsetDateTime.of(dateTime, ZoneOffset.ofHours(-3));
    final String dbAttr = c.convertToDatabaseColumn(ldt);
    assertThat(dbAttr, is("2010-10-12 12:30:33-03:00"));
  }

  @Test
  public void testReverseOffsetDateTime() {
    final OffsetDateTimeConverter c = new OffsetDateTimeConverter();
    final LocalDateTime dateTime = LocalDateTime.of(2010, 10, 12, 12, 30, 33, 0);
    final OffsetDateTime ldt = OffsetDateTime.of(dateTime, ZoneOffset.ofHours(-3));
    final OffsetDateTime dbAttr = c.convertToEntityAttribute("2010-10-12 12:30:33-03:00");
    assertThat(dbAttr, is(dbAttr));
  }

  @Test
  public void testZonedDateTime() {
    final ZonedDateTimeConverter c = new ZonedDateTimeConverter();
    final LocalDateTime dateTime = LocalDateTime.of(2010, 10, 12, 12, 30, 33);
    final ZonedDateTime ldt = ZonedDateTime.of(dateTime, ZoneId.of("Africa/Nairobi"));
    final String dbAttr = c.convertToDatabaseColumn(ldt);
    assertThat(dbAttr, is("2010-10-12 12:30:33[Africa/Nairobi]"));
  }

  @Test
  public void testReverseZonedDateTime() {
    final ZonedDateTimeConverter c = new ZonedDateTimeConverter();
    final LocalDateTime dateTime = LocalDateTime.of(2010, 10, 12, 12, 30, 33, 0);
    final ZonedDateTime ldt = ZonedDateTime.of(dateTime, ZoneId.of("Africa/Nairobi"));
    final ZonedDateTime dbAttr = c.convertToEntityAttribute("2010-10-12 12:30:33[Africa/Nairobi]");
    assertThat(dbAttr, is(dbAttr));
  }

}
