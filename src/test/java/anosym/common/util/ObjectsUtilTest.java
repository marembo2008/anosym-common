package anosym.common.util;

import org.junit.jupiter.api.Test;

import anosym.common.util.ObjectsUtil;

import static anosym.common.util.ObjectsUtil.firstNonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 7, 2016, 4:53:36 AM
 */
public class ObjectsUtilTest {

  @Test
  @SuppressWarnings("null")
  public void verifyFirstIsNotNull() {
    final String value = "78";
    assertThat(firstNonNull(value, () -> null), is(value));
  }

  @Test
  @SuppressWarnings("null")
  public void verifySupplierIsNotNull() {
    final String value = "78";
    assertThat(firstNonNull(null, () -> value), is(value));
  }

  @Test
  @SuppressWarnings("null")
  public void verifyBothValueAndSupplierAreNull() {
    assertThrows(IllegalArgumentException.class, () -> firstNonNull(null, () -> null));
  }

  @Test
  @SuppressWarnings("null")
  public void verifyLastSupplierIsNotNull() {
    final String value = "78";
    assertThat(firstNonNull(null, () -> null, () -> null, () -> null, () -> value), is(value));
  }

  @Test
  @SuppressWarnings("null")
  public void verifyValueAndSuppliersAreNull() {
    assertThrows(IllegalArgumentException.class,
                 () -> firstNonNull(null, () -> null, () -> null, () -> null, () -> null));
  }

  @Test
  public void verifyAllNull() {
    final String nullValue = null;
    final String nullObject = ObjectsUtil.whicheverNonNull(nullValue, nullValue, nullValue);
    assertThat(nullObject, is(nullValue()));
  }

  @Test
  public void verifyFirstNonNull() {
    final String nonNullObject = "non-null";
    final String nullObject = ObjectsUtil.whicheverNonNull(nonNullObject, null, null);
    assertThat(nullObject, is(nonNullObject));
  }

  @Test
  public void verifyLastNonNull() {
    final String nonNullObject = "non-null";
    final String nullObject = ObjectsUtil.whicheverNonNull(null, null, nonNullObject);
    assertThat(nullObject, is(nonNullObject));
  }

  @Test
  public void verifySupplierNonNull() {
    final String nonNullObject = "non-null";
    final String nullObject = ObjectsUtil.whicheverNonNull(() -> null, () -> nonNullObject, () -> null);
    assertThat(nullObject, is(nonNullObject));
  }

  @Test
  public void verifyAllSuppliersReturnNull() {
    final String nullObject = ObjectsUtil.whicheverNonNull(() -> null, () -> null, () -> null);
    assertThat(nullObject, is(nullValue()));
  }

  @Test
  public void verifyANullSupplier() {
    final String nonNullObject = "non-null";
    assertThrows(NullPointerException.class, () -> ObjectsUtil.whicheverNonNull(null, () -> nonNullObject, () -> null));
  }

}
