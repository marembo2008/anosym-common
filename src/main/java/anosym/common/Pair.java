package anosym.common;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 5, 2016, 7:04:54 PM
 */
@Getter
@ToString
@EqualsAndHashCode
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Pair<F, S> {

    @Nullable
    private F first;

    @Nullable
    private S second;

    @Nonnull
    public static <F, S> Pair<F, S> of(@Nullable final F first, @Nullable final S second) {
        return new Pair<>(first, second);
    }
}
