package anosym.common.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo
 */
public class CalendarConverter implements Converter<Calendar, String> {

    private static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @Override
    public String to(@Nonnull final Calendar value, @Nonnull final String... formats) {
        checkNotNull(value, "The calendar value must be null");

        final String formats_[] = formats.length == 0 ? new String[]{DEFAULT_FORMAT} : formats;

        final ImmutableList.Builder<String> errors = ImmutableList.builder();

        return ImmutableList
                .copyOf(formats_)
                .stream()
                .map((format) -> {
                    try {
                        final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
                        return dateFormat.format(value.getTime());
                    } catch (final Exception ex) {
                        errors.add(ex.getLocalizedMessage());
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow(() -> {
                    final String errorMessage = errors.build().stream().collect(Collectors.joining(";"));
                    return new ConverterException(value.getClass(), String.class, errorMessage);
                });
    }

    @Override
    public Calendar from(@Nonnull final String value, @Nonnull final String... formats) {
        checkNotNull(value, "The calendar value must be null");

        final String formats_[] = formats.length == 0 ? new String[]{DEFAULT_FORMAT} : formats;

        if (value.trim().equalsIgnoreCase("NA")) {
            return null;
        }

        final ImmutableList.Builder<String> errors = ImmutableList.builder();

        return ImmutableList
                .copyOf(formats_)
                .stream()
                .map((format) -> {
                    try {
                        final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
                        final Date date = dateFormat.parse(value);
                        final Calendar instance = zeroCalendar();
                        instance.setTime(date);
                        return instance;
                    } catch (final ParseException ex) {
                        errors.add(ex.getLocalizedMessage());
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow(() -> {
                    final String errorMessage = errors.build().stream().collect(Collectors.joining(";"));
                    return new ConverterException(String.class, value.getClass(), errorMessage);
                });

    }

    @Nonnull
    private Calendar zeroCalendar() {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(0);
        return instance;
    }

    @Nonnull
    public static Calendar toTimeZone(@Nonnull final Calendar dateTime, @Nonnull final TimeZone timeZone) {
        final long timeMillis = dateTime.getTimeInMillis();
        final int utcTimeMillis = dateTime.getTimeZone().getRawOffset();
        final long newTimeMillis = (timeMillis - utcTimeMillis) + timeZone.getRawOffset();
        final Calendar newDateTime = Calendar.getInstance(timeZone);
        newDateTime.setTimeInMillis(newTimeMillis);

        return newDateTime;
    }

}
