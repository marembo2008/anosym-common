package anosym.common.jpa;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

import javax.annotation.Nullable;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 15, 2017, 5:00:05 PM
 */
@Converter(autoApply = true)
public class OffsetDateTimeConverter implements AttributeConverter<OffsetDateTime, String> {

  private static final DateTimeFormatter ISO_DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  private static final DateTimeFormatter DATE_TIME_FORMAT = new DateTimeFormatterBuilder()
          .parseCaseInsensitive()
          .append(ISO_DATE_TIME_FORMAT)
          .appendOffsetId()
          .toFormatter(Locale.getDefault(Locale.Category.FORMAT));

  @Override
  @Nullable
  public String convertToDatabaseColumn(@Nullable final OffsetDateTime attribute) {
    if (attribute == null) {
      return null;
    }

    return attribute.format(DATE_TIME_FORMAT);
  }

  @Override
  @Nullable
  public OffsetDateTime convertToEntityAttribute(@Nullable final String dbData) {
    if (dbData == null) {
      return null;
    }

    return OffsetDateTime.parse(dbData, DATE_TIME_FORMAT);
  }

}
