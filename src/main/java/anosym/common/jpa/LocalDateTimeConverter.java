package anosym.common.jpa;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.Nullable;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 15, 2017, 5:00:05 PM
 */
@Converter(autoApply = true)
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, String> {

  private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  @Override
  @Nullable
  public String convertToDatabaseColumn(@Nullable final LocalDateTime attribute) {
    if (attribute == null) {
      return null;
    }

    return attribute.format(DATE_TIME_FORMAT);
  }

  @Override
  @Nullable
  public LocalDateTime convertToEntityAttribute(@Nullable final String dbData) {
    if (dbData == null) {
      return null;
    }

    return LocalDateTime.parse(dbData, DATE_TIME_FORMAT);
  }

}
