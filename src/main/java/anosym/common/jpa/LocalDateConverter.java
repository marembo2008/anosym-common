package anosym.common.jpa;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.annotation.Nullable;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 15, 2017, 5:00:05 PM
 */
@Converter(autoApply = true)
public class LocalDateConverter implements AttributeConverter<LocalDate, String> {

  private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ISO_DATE;

  @Override
  @Nullable
  public String convertToDatabaseColumn(@Nullable final LocalDate attribute) {
    if (attribute == null) {
      return null;
    }

    return attribute.format(DATE_TIME_FORMAT);
  }

  @Override
  @Nullable
  public LocalDate convertToEntityAttribute(@Nullable final String dbData) {
    if (dbData == null) {
      return null;
    }

    return LocalDate.parse(dbData, DATE_TIME_FORMAT);
  }

}
