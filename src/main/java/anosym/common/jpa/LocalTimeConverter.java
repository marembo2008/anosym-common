package anosym.common.jpa;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.Nullable;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 15, 2017, 5:00:05 PM
 */
@Converter(autoApply = true)
public class LocalTimeConverter implements AttributeConverter<LocalTime, String> {

  private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ISO_TIME;

  @Override
  @Nullable
  public String convertToDatabaseColumn(@Nullable final LocalTime attribute) {
    if (attribute == null) {
      return null;
    }

    return attribute.format(DATE_TIME_FORMAT);
  }

  @Override
  @Nullable
  public LocalTime convertToEntityAttribute(@Nullable final String dbData) {
    if (dbData == null) {
      return null;
    }

    return LocalTime.parse(dbData, DATE_TIME_FORMAT);
  }

}
