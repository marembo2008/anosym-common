package anosym.common.jpa;

import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.Nullable;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 15, 2017, 5:00:05 PM
 */
@Converter(autoApply = true)
public class OffsetTimeConverter implements AttributeConverter<OffsetTime, String> {

  private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ISO_OFFSET_TIME;

  @Override
  @Nullable
  public String convertToDatabaseColumn(@Nullable final OffsetTime attribute) {
    if (attribute == null) {
      return null;
    }

    return attribute.format(DATE_TIME_FORMAT);
  }

  @Override
  @Nullable
  public OffsetTime convertToEntityAttribute(@Nullable final String dbData) {
    if (dbData == null) {
      return null;
    }

    return OffsetTime.parse(dbData, DATE_TIME_FORMAT);
  }

}
