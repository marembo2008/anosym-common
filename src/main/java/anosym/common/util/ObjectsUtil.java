package anosym.common.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.ImmutableList;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Sep 7, 2016, 4:48:06 AM
 */
public final class ObjectsUtil {

	public static boolean anyNull(@Nonnull final Object... objs) {
		return Arrays.stream(objs).anyMatch(Objects::isNull);
	}

	public static boolean allNull(@Nonnull final Object... objs) {
		return Arrays.stream(objs).noneMatch(Objects::nonNull);
	}

	public static boolean anyNonNull(@Nonnull final Object... objs) {
		return Arrays.stream(objs).anyMatch(Objects::nonNull);
	}

	public static boolean allNonNull(@Nonnull final Object... objs) {
		return Arrays.stream(objs).noneMatch(Objects::isNull);
	}

	public static <T> T firstNonNull(@Nullable final T obj1, @Nonnull final Supplier<T> possibluNullSupplier) {
		if (obj1 != null) {
			return obj1;
		}

		final T suppliedObject = possibluNullSupplier.get();
		if (suppliedObject != null) {
			return suppliedObject;
		}

		throw new IllegalArgumentException("All arguments must not be null");
	}

	public static <T> T firstNonNull(@Nullable final T obj1, @Nonnull final Supplier<T>... possibluNullSuppliers) {
		if (obj1 != null) {
			return obj1;
		}

		return ImmutableList.copyOf(possibluNullSuppliers).stream().map(Supplier::get).filter(Objects::nonNull)
				.findFirst().orElseThrow(() -> new IllegalArgumentException("All arguments must not be null"));
	}

	@Nullable
	public static <T> T whicheverNonNull(@Nonnull final T... objects) {
		for (final T obj : objects) {
			if (obj != null) {
				return obj;
			}
		}

		return null;
	}

	@Nullable
	@SafeVarargs
	public static <T> T whicheverNonNull(@Nonnull final Supplier<T>... suppliers) {
		return ImmutableList.copyOf(suppliers).stream().map(ObjectsUtil::provideUnchecked).filter(Objects::nonNull)
				.findFirst().orElse(null);
	}

	@Nonnull
	public static List<Field> getFields(@Nonnull final Class<?> clazz) {
		final List<Field> fields = new ArrayList<>();
		getFields(fields, clazz);

		return Collections.unmodifiableList(fields);
	}

	private static void getFields(@Nonnull final List<Field> fields, @Nonnull Class<?> clazz) {
		if (clazz == Object.class) {
			return;
		}

		fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
		getFields(fields, clazz.getSuperclass());
	}

	@Nullable
	private static <T> T provideUnchecked(@Nonnull Supplier<T> supplier) {
		try {
			return supplier.get();
		} catch (final Exception e) {
			return null;
		}
	}

}
