package anosym.common;

import com.google.common.collect.ImmutableList;
import java.util.List;
import javax.annotation.Nonnull;

/**
 *
 * @author mochieng
 */
public enum Language {

    ENGLISH("en", "English"),
    GERMAN("de", "German"),
    SWAHILI("sw", "Swahili"),
    FRENCH("fr", "French"),
    AFAR("aa", "Afar"),
    ABKHAZIAN("ab", "Abkhazian"),
    AVESTAN("ae", "Avestan"),
    AFRIKAANS("af", "Afrikaans"),
    AKAN("ak", "Akan"),
    AMHARIC("am", "Amharic"),
    ARAGONESE("an", "Aragonese"),
    ARABIC("ar", "Arabic"),
    ASSAMESE("as", "Assamese"),
    AVARIC("av", "Avaric"),
    AYMARA("ay", "Aymara"),
    AZERBAIJANI("az", "Azerbaijani"),
    BASHKIR("ba", "Bashkir"),
    BELARUSIAN("be", "Belarusian"),
    BULGARIAN("bg", "Bulgarian"),
    BIHARI("bh", "Bihari"),
    BISLAMA("bi", "Bislama"),
    BAMBARA("bm", "Bambara"),
    BENGALI("bn", "Bengali"),
    TIBETAN("bo", "Tibetan"),
    BRETON("br", "Breton"),
    BOSNIAN("bs", "Bosnian"),
    CATALAN("ca", "Catalan"),
    CHECHEN("ce", "Chechen"),
    CHAMORRO("ch", "Chamorro"),
    CORSICAN("co", "Corsican"),
    CREE("cr", "Cree"),
    CZECH("cs", "Czech"),
    CHURCH_SLAVIC("cu", "Church Slavic"),
    CHUVASH("cv", "Chuvash"),
    WELSH("cy", "Welsh"),
    DANISH("da", "Danish"),
    DIVEHI("dv", "Divehi"),
    DZONGKHA("dz", "Dzongkha"),
    EWE("ee", "Ewe"),
    GREEK("el", "Greek"),
    ESPERANTO("eo", "Esperanto"),
    SPANISH("es", "Spanish"),
    ESTONIAN("et", "Estonian"),
    BASQUE("eu", "Basque"),
    PERSIAN("fa", "Persian"),
    FULAH("ff", "Fulah"),
    FINNISH("fi", "Finnish"),
    FIJIAN("fj", "Fijian"),
    FAROESE("fo", "Faroese"),
    FRISIAN("fy", "Frisian"),
    IRISH("ga", "Irish"),
    SCOTTISH_GAELIC("gd", "Scottish Gaelic"),
    GALLEGAN("gl", "Gallegan"),
    GUARANI("gn", "Guarani"),
    GUJARATI("gu", "Gujarati"),
    MANX("gv", "Manx"),
    HAUSA("ha", "Hausa"),
    HEBREW("he", "Hebrew"),
    HINDI("hi", "Hindi"),
    HIRI_MOTU("ho", "Hiri Motu"),
    CROATIAN("hr", "Croatian"),
    HAITIAN("ht", "Haitian"),
    HUNGARIAN("hu", "Hungarian"),
    ARMENIAN("hy", "Armenian"),
    HERERO("hz", "Herero"),
    INTERLINGUA("ia", "Interlingua"),
    INDONESIAN("id", "Indonesian"),
    INTERLINGUE("ie", "Interlingue"),
    IGBO("ig", "Igbo"),
    SICHUAN_YI("ii", "Sichuan Yi"),
    INUPIAQ("ik", "Inupiaq"),
    IDO("io", "Ido"),
    ICELANDIC("is", "Icelandic"),
    ITALIAN("it", "Italian"),
    INUKTITUT("iu", "Inuktitut"),
    JAPANESE("ja", "Japanese"),
    YIDDISH("ji", "Yiddish"),
    JAVANESE("jv", "Javanese"),
    GEORGIAN("ka", "Georgian"),
    KONGO("kg", "Kongo"),
    KIKUYU("ki", "Kikuyu"),
    KWANYAMA("kj", "Kwanyama"),
    KAZAKH("kk", "Kazakh"),
    GREENLANDIC("kl", "Greenlandic"),
    KHMER("km", "Khmer"),
    KANNADA("kn", "Kannada"),
    KOREAN("ko", "Korean"),
    KANURI("kr", "Kanuri"),
    KASHMIRI("ks", "Kashmiri"),
    KURDISH("ku", "Kurdish"),
    KOMI("kv", "Komi"),
    CORNISH("kw", "Cornish"),
    KIRGHIZ("ky", "Kirghiz"),
    LATIN("la", "Latin"),
    LUXEMBOURGISH("lb", "Luxembourgish"),
    GANDA("lg", "Ganda"),
    LIMBURGISH("li", "Limburgish"),
    LINGALA("ln", "Lingala"),
    LAO("lo", "Lao"),
    LITHUANIAN("lt", "Lithuanian"),
    LUBA_KATANGA("lu", "Luba-Katanga"),
    LATVIAN("lv", "Latvian"),
    MALAGASY("mg", "Malagasy"),
    MARSHALLESE("mh", "Marshallese"),
    MAORI("mi", "Maori"),
    MACEDONIAN("mk", "Macedonian"),
    MALAYALAM("ml", "Malayalam"),
    MONGOLIAN("mn", "Mongolian"),
    MOLDAVIAN("mo", "Moldavian"),
    MARATHI("mr", "Marathi"),
    MALAY("ms", "Malay"),
    MALTESE("mt", "Maltese"),
    BURMESE("my", "Burmese"),
    NAURU("na", "Nauru"),
    NORWEGIAN_BOKM_L("nb", "Norwegian Bokmål"),
    NORTH_NDEBELE("nd", "North Ndebele"),
    NEPALI("ne", "Nepali"),
    NDONGA("ng", "Ndonga"),
    DUTCH("nl", "Dutch"),
    NORWEGIAN_NYNORSK("nn", "Norwegian Nynorsk"),
    NORWEGIAN("no", "Norwegian"),
    SOUTH_NDEBELE("nr", "South Ndebele"),
    NAVAJO("nv", "Navajo"),
    NYANJA("ny", "Nyanja"),
    OCCITAN("oc", "Occitan"),
    OJIBWA("oj", "Ojibwa"),
    OROMO("om", "Oromo"),
    ORIYA("or", "Oriya"),
    OSSETIAN("os", "Ossetian"),
    PANJABI("pa", "Panjabi"),
    PALI("pi", "Pali"),
    POLISH("pl", "Polish"),
    PUSHTO("ps", "Pushto"),
    PORTUGUESE("pt", "Portuguese"),
    QUECHUA("qu", "Quechua"),
    RAETO_ROMANCE("rm", "Raeto-Romance"),
    RUNDI("rn", "Rundi"),
    ROMANIAN("ro", "Romanian"),
    RUSSIAN("ru", "Russian"),
    KINYARWANDA("rw", "Kinyarwanda"),
    SANSKRIT("sa", "Sanskrit"),
    SARDINIAN("sc", "Sardinian"),
    SINDHI("sd", "Sindhi"),
    NORTHERN_SAMI("se", "Northern Sami"),
    SANGO("sg", "Sango"),
    SINHALESE("si", "Sinhalese"),
    SLOVAK("sk", "Slovak"),
    SLOVENIAN("sl", "Slovenian"),
    SAMOAN("sm", "Samoan"),
    SHONA("sn", "Shona"),
    SOMALI("so", "Somali"),
    ALBANIAN("sq", "Albanian"),
    SERBIAN("sr", "Serbian"),
    SWATI("ss", "Swati"),
    SOUTHERN_SOTHO("st", "Southern Sotho"),
    SUNDANESE("su", "Sundanese"),
    SWEDISH("sv", "Swedish"),
    TAMIL("ta", "Tamil"),
    TELUGU("te", "Telugu"),
    TAJIK("tg", "Tajik"),
    THAI("th", "Thai"),
    TIGRINYA("ti", "Tigrinya"),
    TURKMEN("tk", "Turkmen"),
    TAGALOG("tl", "Tagalog"),
    TSWANA("tn", "Tswana"),
    TONGA("to", "Tonga"),
    TURKISH("tr", "Turkish"),
    TSONGA("ts", "Tsonga"),
    TATAR("tt", "Tatar"),
    TWI("tw", "Twi"),
    TAHITIAN("ty", "Tahitian"),
    UIGHUR("ug", "Uighur"),
    UKRAINIAN("uk", "Ukrainian"),
    URDU("ur", "Urdu"),
    UZBEK("uz", "Uzbek"),
    VENDA("ve", "Venda"),
    VIETNAMESE("vi", "Vietnamese"),
    VOLAP_K("vo", "Volapük"),
    WALLOON("wa", "Walloon"),
    WOLOF("wo", "Wolof"),
    XHOSA("xh", "Xhosa"),
    YORUBA("yo", "Yoruba"),
    ZHUANG("za", "Zhuang"),
    CHINESE("zh", "Chinese"),
    ZULU("zu", "Zulu");

    private final String isoCode;

    private final String name;

    private final List<Country> countries;

    private Language(final String isoCode, final String name, final Country... countries) {
        this.isoCode = isoCode;
        this.name = name;
        this.countries = ImmutableList.copyOf(countries);
    }

    @Nonnull
    public String getIsoCode() {
        return isoCode;
    }

    @Nonnull
    public String getName() {
        return name;
    }

    @Nonnull
    public static Language fromIsoCode(String isoCode) {
        for (Language l : values()) {
            if (l.isoCode.equalsIgnoreCase(isoCode)) {
                return l;
            }
        }
        throw new IllegalArgumentException("There is no language defined for the specified isocode");
    }

    @Override
    public String toString() {
        return name + " (" + isoCode + ")";
    }

}
