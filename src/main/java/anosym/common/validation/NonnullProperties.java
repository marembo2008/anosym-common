package anosym.common.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Dec 21, 2016, 11:40:50 AM
 */
@Documented
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NonnullPropertiesValidator.class)
public @interface NonnullProperties {

  String message() default "Properties marked as @Nonnull must not be null";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
