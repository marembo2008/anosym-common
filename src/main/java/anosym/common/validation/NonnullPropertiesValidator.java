package anosym.common.validation;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Throwables;

import anosym.common.Pair;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import static anosym.common.util.ObjectsUtil.getFields;
import static java.lang.String.format;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Dec 21, 2016, 11:55:36 AM
 */
public class NonnullPropertiesValidator implements ConstraintValidator<NonnullProperties, Object> {

  @Override
  public void initialize(@Nonnull final NonnullProperties constraintAnnotation) {
  }

  @Override
  public boolean isValid(@Nullable final Object value, @Nonnull final ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }

    if (Collection.class.isAssignableFrom(value.getClass())) {
      return ((Collection) value)
              .stream()
              .allMatch((obj) -> isValid(obj, context));
    }

    final List<Field> nonnullFields = getFields(value.getClass())
            .stream()
            .filter((field) -> field.isAnnotationPresent(Nonnull.class))
            .collect(Collectors.toList());

    final List<Pair<String, Object>> fieldValues = nonnullFields
            .stream()
            .map((field) -> {
              try {
                field.setAccessible(true);
                return Pair.of(field.getName(), field.get(value));
              } catch (final SecurityException | IllegalArgumentException | IllegalAccessException e) {
                throw Throwables.propagate(e);
              }
            })
            .collect(Collectors.toList());

    final boolean isValid = fieldValues.isEmpty() || fieldValues
            .stream()
            .map(Pair::getSecond)
            .allMatch(Objects::nonNull);

    if (!isValid) {
      final String nullNonnullFields = fieldValues
              .stream()
              .filter((field) -> field.getSecond() == null)
              .map((field) -> field.getFirst())
              .collect(Collectors.joining(","));
      final String message = format("Properties marked as @Nonnull must not be null: %s", nullNonnullFields);
      context
              .buildConstraintViolationWithTemplate(message)
              .addConstraintViolation();
    }
    return isValid;
  }

}
