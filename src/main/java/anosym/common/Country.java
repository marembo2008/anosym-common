package anosym.common;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

/**
 *
 * @author marembo
 */
public enum Country {

	AFGHANISTAN("AF", "AFG", "Afghanistan", "93", timezones("Asia/Kabul")), // 0
	ALBANIA("AL", "ALB", "Albania", "355", timezones("Europe/Tirane")), // 1
	ALGERIA("DZ", "DZA", "Algeria", "213", timezones("Africa/Algiers")), // 2
	AMERICAN_SAMOA("AS", "ASM", "American Samoa", "1 684", timezones("Pacific/Pago_Pago")), // 3
	ANDORRA("AD", "AND", "Andorra", "376", timezones("Europe/Andorra")), // 4
	ANGOLA("AO", "AGO", "Angola", "244", timezones("Africa/Luanda")), // 5
	ANGUILLA("AI", "AIA", "Anguilla", "1 264", timezones("America/Anguilla")), // 6
	ANTARCTICA("AQ", "ATA", "Antarctica", "672",
			timezones("Antarctica/McMurdo", "Antarctica/Rothera", "Antarctica/Palmer", "Antarctica/Mawson",
					"Antarctica/Davis", "Antarctica/Casey", "Antarctica/Vostok", "Antarctica/DumontDUrville",
					"Antarctica/Syowa", "Antarctica/Troll")), // 7
	ANTIGUA_AND_BARBUDA("AG", "ATG", "Antigua and Barbuda", "1 268", timezones("America/Antigua")), // 8
	ARGENTINA("AR", "ARG", "Argentina", "54",
			timezones("America/Argentina/Buenos_Aires", "America/Argentina/Cordoba", "America/Argentina/Salta",
					"America/Argentina/Jujuy", "America/Argentina/Tucuman", "America/Argentina/Catamarca",
					"America/Argentina/La_Rioja", "America/Argentina/San_Juan", "America/Argentina/Mendoza",
					"America/Argentina/San_Luis", "America/Argentina/Rio_Gallegos", "America/Argentina/Ushuaia")), // 9
	ARMENIA("AM", "ARM", "Armenia", "374", timezones("Asia/Yerevan")), // 10
	ARUBA("AW", "ABW", "Aruba", "297", timezones("America/Aruba")), // 11
	AUSTRALIA("AU", "AUS", "Australia", "61",
			timezones("Australia/Lord_Howe", "Antarctica/Macquarie", "Australia/Hobart", "Australia/Currie",
					"Australia/Melbourne", "Australia/Sydney", "Australia/Broken_Hill", "Australia/Brisbane",
					"Australia/Lindeman", "Australia/Adelaide", "Australia/Darwin", "Australia/Perth",
					"Australia/Eucla")), // 12
	AUSTRIA("AT", "AUT", "Austria", "43", timezones("Europe/Vienna")), // 13
	AZERBAIJAN("AZ", "AZE", "Azerbaijan", "994", timezones("Asia/Baku")), // 14
	BAHAMAS("BS", "BHS", "Bahamas", "1 242", timezones("America/Nassau")), // 15
	BAHRAIN("BH", "BHR", "Bahrain", "973", timezones("Asia/Bahrain")), // 16
	BANGLADESH("BD", "BGD", "Bangladesh", "880", timezones("Asia/Dhaka")), // 17
	BARBADOS("BB", "BRB", "Barbados", "1 246", timezones("America/Barbados")), // 18
	BELARUS("BY", "BLR", "Belarus", "375", timezones("Europe/Minsk")), // 19
	BELGIUM("BE", "BEL", "Belgium", "32", timezones("Europe/Brussels")), // 20
	BELIZE("BZ", "BLZ", "Belize", "501", timezones("America/Belize")), // 21
	BENIN("BJ", "BEN", "Benin", "229", timezones("Africa/Porto-Novo")), // 22
	BERMUDA("BM", "BMU", "Bermuda", "1 441", timezones("Atlantic/Bermuda")), // 23
	BHUTAN("BT", "BTN", "Bhutan", "975", timezones("Asia/Thimphu")), // 24
	BOLIVIA("BO", "BOL", "Bolivia", "591", timezones("America/La_Paz")), // 25
	BOSNIA_AND_HERZEGOVINA("BA", "BIH", "Bosnia and Herzegovina", "387", timezones("Europe/Sarajevo")), // 26
	BOTSWANA("BW", "BWA", "Botswana", "267", timezones("Africa/Gaborone")), // 27
	BRAZIL("BR", "BRA", "Brazil", "55",
			timezones("America/Noronha", "America/Belem", "America/Fortaleza", "America/Recife", "America/Araguaina",
					"America/Maceio", "America/Bahia", "America/Sao_Paulo", "America/Campo_Grande", "America/Cuiaba",
					"America/Santarem", "America/Porto_Velho", "America/Boa_Vista", "America/Manaus",
					"America/Eirunepe", "America/Rio_Branco")), // 28
	BRITISH_INDIAN_OCEAN_TERRITORY("IO", "IOT", "British Indian Ocean Territory", " ", timezones("Indian/Chagos")), // 29
	BRITISH_VIRGIN_ISLANDS("VG", "VGB", "British Virgin Islands", "1 284", timezones("America/Tortola")), // 30
	BRUNEI("BN", "BRN", "Brunei", "673", timezones("Asia/Brunei")), // 31
	BULGARIA("BG", "BGR", "Bulgaria", "359", timezones("Europe/Sofia")), // 32
	BURKINA_FASO("BF", "BFA", "Burkina Faso", "226", timezones("Africa/Ouagadougou")), // 33
	REPUBLIC_OF_THE_UNION_OF_MYANMAR("MM", "MMR", "Burma (Republic of the Union of Myanmar)", "95",
			timezones("Asia/Rangoon")), // 34
	BURUNDI("BI", "BDI", "Burundi", "257", timezones("Africa/Bujumbura")), // 35
	CAMBODIA("KH", "KHM", "Cambodia", "855", timezones("Asia/Phnom_Penh")), // 36
	CAMEROON("CM", "CMR", "Cameroon", "237", timezones("Africa/Douala")), // 37
	CANADA("CA", "CAN", "Canada", "1",
			timezones("America/St_Johns", "America/Halifax", "America/Glace_Bay", "America/Moncton",
					"America/Goose_Bay", "America/Blanc-Sablon", "America/Toronto", "America/Nipigon",
					"America/Thunder_Bay", "America/Iqaluit", "America/Pangnirtung", "America/Resolute",
					"America/Atikokan", "America/Rankin_Inlet", "America/Winnipeg", "America/Rainy_River",
					"America/Regina", "America/Swift_Current", "America/Edmonton", "America/Cambridge_Bay",
					"America/Yellowknife", "America/Inuvik", "America/Creston", "America/Dawson_Creek",
					"America/Fort_Nelson", "America/Vancouver", "America/Whitehorse", "America/Dawson")), // 38
	CAPE_VERDE("CV", "CPV", "Cape Verde", "238", timezones("Atlantic/Cape_Verde")), // 39
	CAYMAN_ISLANDS("KY", "CYM", "Cayman Islands", "1 345", timezones("America/Cayman")), // 40
	CENTRAL_AFRICAN_REPUBLIC("CF", "CAF", "Central African Republic", "236", timezones("Africa/Bangui")), // 41
	CHAD("TD", "TCD", "Chad", "235", timezones("Africa/Ndjamena")), // 42
	CHILE("CL", "CHL", "Chile", "56", timezones("America/Santiago", "Pacific/Easter")), // 43
	CHINA("CN", "CHN", "China", "86", timezones("Asia/Shanghai", "Asia/Urumqi")), // 44
	CHRISTMAS_ISLAND("CX", "CXR", "Christmas Island", "61", timezones("Indian/Christmas")), // 45
	COCOS_KEELING_ISLANDS("CC", "CCK", "Cocos (Keeling) Islands", "61", timezones("Indian/Cocos")), // 46
	COLOMBIA("CO", "COL", "Colombia", "57", timezones("America/Bogota")), // 47
	COMOROS("KM", "COM", "Comoros", "269", timezones("Indian/Comoro")), // 48
	COOK_ISLANDS("CK", "COK", "Cook Islands", "682", timezones("Pacific/Rarotonga")), // 49
	COSTA_RICA("CR", "CRC", "Costa Rica", "506", timezones("America/Costa_Rica")), // 50
	CROATIA("HR", "HRV", "Croatia", "385", timezones("Europe/Zagreb")), // 51
	CUBA("CU", "CUB", "Cuba", "53", timezones("America/Havana")), // 52
	CYPRUS("CY", "CYP", "Cyprus", "357", timezones("Asia/Nicosia")), // 53
	CZECH_REPUBLIC("CZ", "CZE", "Czech Republic", "420", timezones("Europe/Prague")), // 54
	DEMOCRATIC_REPUBLIC_OF_THE_CONGO("CD", "COD", "Democratic Republic of the Congo", "243",
			timezones("Africa/Kinshasa", "Africa/Lubumbashi")), // 55
	DENMARK("DK", "DNK", "Denmark", "45", timezones("Europe/Copenhagen")), // 56
	DJIBOUTI("DJ", "DJI", "Djibouti", "253", timezones("Africa/Djibouti")), // 57
	DOMINICA("DM", "DMA", "Dominica", "1 767", timezones("America/Dominica")), // 58
	DOMINICAN_REPUBLIC("DO", "DOM", "Dominican Republic", "1 809", timezones("America/Santo_Domingo")), // 59
	ECUADOR("EC", "ECU", "Ecuador", "593", timezones("America/Guayaquil", "Pacific/Galapagos")), // 60
	EGYPT("EG", "EGY", "Egypt", "20", timezones("Africa/Cairo")), // 61
	EL_SALVADOR("SV", "SLV", "El Salvador", "503", timezones("America/El_Salvador")), // 62
	EQUATORIAL_GUINEA("GQ", "GNQ", "Equatorial Guinea", "240", timezones("Africa/Malabo")), // 63
	ERITREA("ER", "ERI", "Eritrea", "291", timezones("Africa/Asmara")), // 64
	ESTONIA("EE", "EST", "Estonia", "372", timezones("Europe/Tallinn")), // 65
	ETHIOPIA("ET", "ETH", "Ethiopia", "251", timezones("Africa/Addis_Ababa")), // 66
	FALKLAND_ISLANDS("FK", "FLK", "Falkland Islands", "500", timezones("Atlantic/Stanley")), // 67
	FAROE_ISLANDS("FO", "FRO", "Faroe Islands", "298", timezones("Atlantic/Faroe")), // 68
	FIJI("FJ", "FJI", "Fiji", "679", timezones("Pacific/Fiji")), // 69
	FINLAND("FI", "FIN", "Finland", "358", timezones("Europe/Helsinki")), // 70
	FRANCE("FR", "FRA", "France", "33", timezones("Europe/Paris")), // 71
	FRENCH_POLYNESIA("PF", "PYF", "French Polynesia", "689",
			timezones("Pacific/Tahiti", "Pacific/Marquesas", "Pacific/Gambier")), // 72
	GABON("GA", "GAB", "Gabon", "241", timezones("Africa/Libreville")), // 73
	GAMBIA("GM", "GMB", "Gambia", "220", timezones("Africa/Banjul")), // 74
	GAZA_STRIP(" ", " ", "Gaza Strip", "970", timezones()), // 75
	GEORGIA("GE", "GEO", "Georgia", "995", timezones("Asia/Tbilisi")), // 76
	GERMANY("DE", "DEU", "Germany", "49", timezones("Europe/Berlin", "Europe/Busingen")), // 77
	GHANA("GH", "GHA", "Ghana", "233", timezones("Africa/Accra")), // 78
	GIBRALTAR("GI", "GIB", "Gibraltar", "350", timezones("Europe/Gibraltar")), // 79
	GREECE("GR", "GRC", "Greece", "30", timezones("Europe/Athens")), // 80
	GREENLAND("GL", "GRL", "Greenland", "299",
			timezones("America/Godthab", "America/Danmarkshavn", "America/Scoresbysund", "America/Thule")), // 81
	GRENADA("GD", "GRD", "Grenada", "1 473", timezones("America/Grenada")), // 82
	GUAM("GU", "GUM", "Guam", "1 671", timezones("Pacific/Guam")), // 83
	GUATEMALA("GT", "GTM", "Guatemala", "502", timezones("America/Guatemala")), // 84
	GUINEA("GN", "GIN", "Guinea", "224", timezones("Africa/Conakry")), // 85
	GUINEA_BISSAU("GW", "GNB", "Guinea-Bissau", "245", timezones("Africa/Bissau")), // 86
	GUYANA("GY", "GUY", "Guyana", "592", timezones("America/Guyana")), // 87
	HAITI("HT", "HTI", "Haiti", "509", timezones("America/Port-au-Prince")), // 88
	HOLY_SEE_VATICAN_CITY_("VA", "VAT", "Holy See (Vatican City)", "39", timezones("Europe/Vatican")), // 89
	HONDURAS("HN", "HND", "Honduras", "504", timezones("America/Tegucigalpa")), // 90
	HONG_KONG("HK", "HKG", "Hong Kong", "852", timezones("Asia/Hong_Kong")), // 91
	HUNGARY("HU", "HUN", "Hungary", "36", timezones("Europe/Budapest")), // 92
	ICELAND("IS", "IS", "Iceland", "354", timezones("Atlantic/Reykjavik")), // 93
	INDIA("IN", "IND", "India", "91", timezones("Asia/Kolkata")), // 94
	INDONESIA("ID", "IDN", "Indonesia", "62",
			timezones("Asia/Jakarta", "Asia/Pontianak", "Asia/Makassar", "Asia/Jayapura")), // 95
	IRAN("IR", "IRN", "Iran", "98", timezones("Asia/Tehran")), // 96
	IRAQ("IQ", "IRQ", "Iraq", "964", timezones("Asia/Baghdad")), // 97
	IRELAND("IE", "IRL", "Ireland", "353", timezones("Europe/Dublin")), // 98
	ISLE_OF_MAN("IM", "IMN", "Isle of Man", "44", timezones("Europe/Isle_of_Man")), // 99
	ISRAEL("IL", "ISR", "Israel", "972", timezones("Asia/Jerusalem")), // 100
	ITALY("IT", "ITA", "Italy", "39", timezones("Europe/Rome")), // 101
	IVORY_COAST("CI", "CIV", "Ivory Coast", "225", timezones("Africa/Abidjan")), // 102
	JAMAICA("JM", "JAM", "Jamaica", "1 876", timezones("America/Jamaica")), // 103
	JAPAN("JP", "JPN", "Japan", "81", timezones("Asia/Tokyo")), // 104
	JERSEY("JE", "JEY", "Jersey", " ", timezones("Europe/Jersey")), // 105
	JORDAN("JO", "JOR", "Jordan", "962", timezones("Asia/Amman")), // 106
	KAZAKHSTAN("KZ", "KAZ", "Kazakhstan", "7",
			timezones("Asia/Almaty", "Asia/Qyzylorda", "Asia/Aqtobe", "Asia/Aqtau", "Asia/Oral")), // 107
	KENYA("KE", "KEN", "Kenya", "254", timezones("Africa/Nairobi")), // 108
	KIRIBATI("KI", "KIR", "Kiribati", "686", timezones("Pacific/Tarawa", "Pacific/Enderbury", "Pacific/Kiritimati")), // 109
	KOSOVO(" ", " ", "Kosovo", "381", timezones()), // 110
	KUWAIT("KW", "KWT", "Kuwait", "965", timezones("Asia/Kuwait")), // 111
	KYRGYZSTAN("KG", "KGZ", "Kyrgyzstan", "996", timezones("Asia/Bishkek")), // 112
	LAOS("LA", "LAO", "Laos", "856", timezones("Asia/Vientiane")), // 113
	LATVIA("LV", "LVA", "Latvia", "371", timezones("Europe/Riga")), // 114
	LEBANON("LB", "LBN", "Lebanon", "961", timezones("Asia/Beirut")), // 115
	LESOTHO("LS", "LSO", "Lesotho", "266", timezones("Africa/Maseru")), // 116
	LIBERIA("LR", "LBR", "Liberia", "231", timezones("Africa/Monrovia")), // 117
	LIBYA("LY", "LBY", "Libya", "218", timezones("Africa/Tripoli")), // 118
	LIECHTENSTEIN("LI", "LIE", "Liechtenstein", "423", timezones("Europe/Vaduz")), // 119
	LITHUANIA("LT", "LTU", "Lithuania", "370", timezones("Europe/Vilnius")), // 120
	LUXEMBOURG("LU", "LUX", "Luxembourg", "352", timezones("Europe/Luxembourg")), // 121
	MACAU("MO", "MAC", "Macau", "853", timezones("Asia/Macau")), // 122
	MACEDONIA("MK", "MKD", "Macedonia", "389", timezones("Europe/Skopje")), // 123
	MADAGASCAR("MG", "MDG", "Madagascar", "261", timezones("Indian/Antananarivo")), // 124
	MALAWI("MW", "MWI", "Malawi", "265", timezones("Africa/Blantyre")), // 125
	MALAYSIA("MY", "MYS", "Malaysia", "60", timezones("Asia/Kuala_Lumpur", "Asia/Kuching")), // 126
	MALDIVES("MV", "MDV", "Maldives", "960", timezones("Indian/Maldives")), // 127
	MALI("ML", "MLI", "Mali", "223", timezones("Africa/Bamako")), // 128
	MALTA("MT", "MLT", "Malta", "356", timezones("Europe/Malta")), // 129
	MARSHALL_ISLANDS("MH", "MHL", "Marshall Islands", "692", timezones("Pacific/Majuro", "Pacific/Kwajalein")), // 130
	MAURITANIA("MR", "MRT", "Mauritania", "222", timezones("Africa/Nouakchott")), // 131
	MAURITIUS("MU", "MUS", "Mauritius", "230", timezones("Indian/Mauritius")), // 132
	MAYOTTE("YT", "MYT", "Mayotte", "262", timezones("Indian/Mayotte")), // 133
	MEXICO("MX", "MEX", "Mexico", "52",
			timezones("America/Mexico_City", "America/Cancun", "America/Merida", "America/Monterrey",
					"America/Matamoros", "America/Mazatlan", "America/Chihuahua", "America/Ojinaga",
					"America/Hermosillo", "America/Tijuana", "America/Bahia_Banderas")), // 134
	MICRONESIA("FM", "FSM", "Micronesia", "691", timezones("Pacific/Chuuk", "Pacific/Pohnpei", "Pacific/Kosrae")), // 135
	MOLDOVA("MD", "MDA", "Moldova", "373", timezones("Europe/Chisinau")), // 136
	MONACO("MC", "MCO", "Monaco", "377", timezones("Europe/Monaco")), // 137
	MONGOLIA("MN", "MNG", "Mongolia", "976", timezones("Asia/Ulaanbaatar", "Asia/Hovd", "Asia/Choibalsan")), // 138
	MONTENEGRO("ME", "MNE", "Montenegro", "382", timezones("Europe/Podgorica")), // 139
	MONTSERRAT("MS", "MSR", "Montserrat", "1 664", timezones("America/Montserrat")), // 140
	MOROCCO("MA", "MAR", "Morocco", "212", timezones("Africa/Casablanca")), // 141
	MOZAMBIQUE("MZ", "MOZ", "Mozambique", "258", timezones("Africa/Maputo")), // 142
	NAMIBIA("NA", "NAM", "Namibia", "264", timezones("Africa/Windhoek")), // 143
	NAURU("NR", "NRU", "Nauru", "674", timezones("Pacific/Nauru")), // 144
	NEPAL("NP", "NPL", "Nepal", "977", timezones("Asia/Kathmandu")), // 145
	NETHERLANDS("NL", "NLD", "Netherlands", "31", timezones("Europe/Amsterdam")), // 146
	NETHERLANDS_ANTILLES("AN", "ANT", "Netherlands Antilles", "599", timezones()), // 147
	NEW_CALEDONIA("NC", "NCL", "New Caledonia", "687", timezones("Pacific/Noumea")), // 148
	NEW_ZEALAND("NZ", "NZL", "New Zealand", "64", timezones("Pacific/Auckland", "Pacific/Chatham")), // 149
	NICARAGUA("NI", "NIC", "Nicaragua", "505", timezones("America/Managua")), // 150
	NIGER("NE", "NER", "Niger", "227", timezones("Africa/Niamey")), // 151
	NIGERIA("NG", "NGA", "Nigeria", "234", timezones("Africa/Lagos")), // 152
	NIUE("NU", "NIU", "Niue", "683", timezones("Pacific/Niue")), // 153
	NORFOLK_ISLAND(" ", "NFK", "Norfolk Island", "672", timezones()), // 154
	NORTH_KOREA("KP", "PRK", "North Korea", "850", timezones("Asia/Pyongyang")), // 155
	NORTHERN_MARIANA_ISLANDS("MP", "MNP", "Northern Mariana Islands", "1 670", timezones("Pacific/Saipan")), // 156
	NORWAY("NO", "NOR", "Norway", "47", timezones("Europe/Oslo")), // 157
	OMAN("OM", "OMN", "Oman", "968", timezones("Asia/Muscat")), // 158
	PAKISTAN("PK", "PAK", "Pakistan", "92", timezones("Asia/Karachi")), // 159
	PALAU("PW", "PLW", "Palau", "680", timezones("Pacific/Palau")), // 160
	PANAMA("PA", "PAN", "Panama", "507", timezones("America/Panama")), // 161
	PAPUA_NEW_GUINEA("PG", "PNG", "Papua New Guinea", "675", timezones("Pacific/Port_Moresby", "Pacific/Bougainville")), // 162
	PARAGUAY("PY", "PRY", "Paraguay", "595", timezones("America/Asuncion")), // 163
	PERU("PE", "PER", "Peru", "51", timezones("America/Lima")), // 164
	PHILIPPINES("PH", "PHL", "Philippines", "63", timezones("Asia/Manila")), // 165
	PITCAIRN_ISLANDS("PN", "PCN", "Pitcairn Islands", "870", timezones("Pacific/Pitcairn")), // 166
	POLAND("PL", "POL", "Poland", "48", timezones("Europe/Warsaw")), // 167
	PORTUGAL("PT", "PRT", "Portugal", "351", timezones("Europe/Lisbon", "Atlantic/Madeira", "Atlantic/Azores")), // 168
	PUERTO_RICO("PR", "PRI", "Puerto Rico", "1", timezones("America/Puerto_Rico")), // 169
	QATAR("QA", "QAT", "Qatar", "974", timezones("Asia/Qatar")), // 170
	REPUBLIC_OF_THE_CONGO("CG", "COG", "Republic of the Congo", "242", timezones("Africa/Brazzaville")), // 171
	ROMANIA("RO", "ROU", "Romania", "40", timezones("Europe/Bucharest")), // 172
	RUSSIA("RU", "RUS", "Russia", "7",
			timezones("Europe/Kaliningrad", "Europe/Moscow", "Europe/Simferopol", "Europe/Volgograd", "Europe/Samara",
					"Asia/Yekaterinburg", "Asia/Omsk", "Asia/Novosibirsk", "Asia/Novokuznetsk", "Asia/Krasnoyarsk",
					"Asia/Irkutsk", "Asia/Chita", "Asia/Yakutsk", "Asia/Khandyga", "Asia/Vladivostok", "Asia/Sakhalin",
					"Asia/Ust-Nera", "Asia/Magadan", "Asia/Srednekolymsk", "Asia/Kamchatka", "Asia/Anadyr")), // 173
	RWANDA("RW", "RWA", "Rwanda", "250", timezones("Africa/Kigali")), // 174
	SAINT_BARTHELEMY("BL", "BLM", "Saint Barthelemy", "590", timezones("America/St_Barthelemy")), // 175
	SAINT_HELENA("SH", "SHN", "Saint Helena", "290", timezones("Atlantic/St_Helena")), // 176
	SAINT_KITTS_AND_NEVIS("KN", "KNA", "Saint Kitts and Nevis", "1 869", timezones("America/St_Kitts")), // 177
	SAINT_LUCIA("LC", "LCA", "Saint Lucia", "1 758", timezones("America/St_Lucia")), // 178
	SAINT_MARTIN("MF", "MAF", "Saint Martin", "1 599", timezones("America/Marigot")), // 179
	SAINT_PIERRE_AND_MIQUELON("PM", "SPM", "Saint Pierre and Miquelon", "508", timezones("America/Miquelon")), // 180
	SAINT_VINCENT_AND_THE_GRENADINES("VC", "VCT", "Saint Vincent and the Grenadines", "1 784",
			timezones("America/St_Vincent")), // 181
	SAMOA("WS", "WSM", "Samoa", "685", timezones("Pacific/Apia")), // 182
	SAN_MARINO("SM", "SMR", "San Marino", "378", timezones("Europe/San_Marino")), // 183
	SAO_TOME_AND_PRINCIPE("ST", "STP", "Sao Tome and Principe", "239", timezones("Africa/Sao_Tome")), // 184
	SAUDI_ARABIA("SA", "SAU", "Saudi Arabia", "966", timezones("Asia/Riyadh")), // 185
	SENEGAL("SN", "SEN", "Senegal", "221", timezones("Africa/Dakar")), // 186
	SERBIA("RS", "SRB", "Serbia", "381", timezones("Europe/Belgrade")), // 187
	SEYCHELLES("SC", "SYC", "Seychelles", "248", timezones("Indian/Mahe")), // 188
	SIERRA_LEONE("SL", "SLE", "Sierra Leone", "232", timezones("Africa/Freetown")), // 189
	SINGAPORE("SG", "SGP", "Singapore", "65", timezones("Asia/Singapore")), // 190
	SLOVAKIA("SK", "SVK", "Slovakia", "421", timezones("Europe/Bratislava")), // 191
	SLOVENIA("SI", "SVN", "Slovenia", "386", timezones("Europe/Ljubljana")), // 192
	SOLOMON_ISLANDS("SB", "SLB", "Solomon Islands", "677", timezones("Pacific/Guadalcanal")), // 193
	SOMALIA("SO", "SOM", "Somalia", "252", timezones("Africa/Mogadishu")), // 194
	SOUTH_AFRICA("ZA", "ZAF", "South Africa", "27", timezones("Africa/Johannesburg")), // 195
	SOUTH_KOREA("KR", "KOR", "South Korea", "82", timezones("Asia/Seoul")), // 196
	SOUTH_SUDAN("SS", "SSD", "South Sudan", "211", timezones("Africa/Juba")), // 197
	SPAIN("ES", "ESP", "Spain", "34", timezones("Europe/Madrid", "Africa/Ceuta", "Atlantic/Canary")), // 198
	SRI_LANKA("LK", "LKA", "Sri Lanka", "94", timezones("Asia/Colombo")), // 199
	SUDAN("SD", "SDN", "Sudan", "249", timezones("Africa/Khartoum")), // 200
	SURINAME("SR", "SUR", "Suriname", "597", timezones("America/Paramaribo")), // 201
	SVALBARD("SJ", "SJM", "Svalbard", " ", timezones("Arctic/Longyearbyen")), // 202
	SWAZILAND("SZ", "SWZ", "Swaziland", "268", timezones("Africa/Mbabane")), // 203
	SWEDEN("SE", "SWE", "Sweden", "46", timezones("Europe/Stockholm")), // 204
	SWITZERLAND("CH", "CHE", "Switzerland", "41", timezones("Europe/Zurich")), // 205
	SYRIA("SY", "SYR", "Syria", "963", timezones("Asia/Damascus")), // 206
	TAIWAN("TW", "TWN", "Taiwan", "886", timezones("Asia/Taipei")), // 207
	TAJIKISTAN("TJ", "TJK", "Tajikistan", "992", timezones("Asia/Dushanbe")), // 208
	TANZANIA("TZ", "TZA", "Tanzania", "255", timezones("Africa/Dar_es_Salaam")), // 209
	THAILAND("TH", "THA", "Thailand", "66", timezones("Asia/Bangkok")), // 210
	TIMOR_LESTE("TL", "TLS", "Timor-Leste", "670", timezones("Asia/Dili")), // 211
	TOGO("TG", "TGO", "Togo", "228", timezones("Africa/Lome")), // 212
	TOKELAU("TK", "TKL", "Tokelau", "690", timezones("Pacific/Fakaofo")), // 213
	TONGA("TO", "TON", "Tonga", "676", timezones("Pacific/Tongatapu")), // 214
	TRINIDAD_AND_TOBAGO("TT", "TTO", "Trinidad and Tobago", "1 868", timezones("America/Port_of_Spain")), // 215
	TUNISIA("TN", "TUN", "Tunisia", "216", timezones("Africa/Tunis")), // 216
	TURKEY("TR", "TUR", "Turkey", "90", timezones("Europe/Istanbul")), // 217
	TURKMENISTAN("TM", "TKM", "Turkmenistan", "993", timezones("Asia/Ashgabat")), // 218
	TURKS_AND_CAICOS_ISLANDS("TC", "TCA", "Turks and Caicos Islands", "1 649", timezones("America/Grand_Turk")), // 219
	TUVALU("TV", "TUV", "Tuvalu", "688", timezones("Pacific/Funafuti")), // 220
	UGANDA("UG", "UGA", "Uganda", "256", timezones("Africa/Kampala")), // 221
	UKRAINE("UA", "UKR", "Ukraine", "380", timezones("Europe/Kiev", "Europe/Uzhgorod", "Europe/Zaporozhye")), // 222
	UNITED_ARAB_EMIRATES("AE", "ARE", "United Arab Emirates", "971", timezones("Asia/Dubai")), // 223
	UNITED_KINGDOM("GB", "GBR", "United Kingdom", "44", timezones("Europe/London")), // 224
	UNITED_STATES("US", "USA", "United States", "1", timezones("America/New_York", //
			"America/Detroit", //
			"America/Kentucky/Louisville", //
			"America/Kentucky/Monticello", //
			"America/Indiana/Indianapolis", //
			"America/Indiana/Vincennes", "America/Indiana/Winamac", "America/Indiana/Marengo",
			"America/Indiana/Petersburg", "America/Indiana/Vevay", "America/Chicago", "America/Indiana/Tell_City",
			"America/Indiana/Knox", "America/Menominee", "America/North_Dakota/Center",
			"America/North_Dakota/New_Salem", "America/North_Dakota/Beulah", "America/Denver", "America/Boise",
			"America/Phoenix", "America/Los_Angeles", "America/Anchorage", "America/Juneau", "America/Sitka",
			"America/Metlakatla", "America/Yakutat", "America/Nome", "America/Adak", "Pacific/Honolulu")), // 225
	URUGUAY("UY", "URY", "Uruguay", "598", timezones("America/Montevideo")), // 226
	US_VIRGIN_ISLANDS("VI", "VIR", "US Virgin Islands", "1 340", timezones("America/St_Thomas")), // 227
	UZBEKISTAN("UZ", "UZB", "Uzbekistan", "998", timezones("Asia/Samarkand", "Asia/Tashkent")), // 228
	VANUATU("VU", "VUT", "Vanuatu", "678", timezones("Pacific/Efate")), // 229
	VENEZUELA("VE", "VEN", "Venezuela", "58", timezones("America/Caracas")), // 230
	VIETNAM("VN", "VNM", "Vietnam", "84", timezones("Asia/Ho_Chi_Minh")), // 231
	WALLIS_AND_FUTUNA("WF", "WLF", "Wallis and Futuna", "681", timezones("Pacific/Wallis")), // 232
	WEST_BANK(" ", " ", "West Bank", "970", timezones()), // 233
	WESTERN_SAHARA("EH", "ESH", "Western Sahara", " ", timezones("Africa/El_Aaiun")), // 234
	YEMEN("YE", "YEM", "Yemen", "967", timezones("Asia/Aden")), // 235
	ZAMBIA("ZM", "ZMB", "Zambia", "260", timezones("Africa/Lusaka")), // 236
	ZIMBABWE("ZW", "ZWE", "Zimbabwe", "263", timezones("Africa/Harare")), // 237

	// Special handling of eu country regions
	EUROPEAN_UNION("EU", "EU", "European Union", "300", timezones()), // 238

	// Special handling of all countries, grouped as one
	INTERNATIONAL_COUNTRY("IC", "ICS", "International Country", "400", timezones()); // 239

	public static final List<Country> EUROPEAN_COUNTRIES = ImmutableList.of(ALBANIA, ANDORRA, ARMENIA, AUSTRIA,
			AZERBAIJAN, BELARUS, BELGIUM, BOSNIA_AND_HERZEGOVINA, BULGARIA, CROATIA, CYPRUS, CZECH_REPUBLIC, DENMARK,
			ESTONIA, FINLAND, FRANCE, GEORGIA, GERMANY, GREECE, HUNGARY, ICELAND, IRELAND, ITALY, KAZAKHSTAN, KOSOVO,
			LATVIA, LIECHTENSTEIN, LITHUANIA, LUXEMBOURG, MACEDONIA, MALTA, MOLDOVA, MONACO, MONTENEGRO, NETHERLANDS,
			NORWAY, POLAND, PORTUGAL, ROMANIA, RUSSIA, SAN_MARINO, SERBIA, SLOVAKIA, SLOVENIA, SPAIN, SWEDEN,
			SWITZERLAND, TURKEY, UKRAINE, UNITED_KINGDOM);

	public static final List<Country> EUROPEAN_UNION_COUNTRIES = ImmutableList.of(AUSTRIA, BELGIUM, BULGARIA, CROATIA,
			CYPRUS, CZECH_REPUBLIC, DENMARK, ESTONIA, FINLAND, FRANCE, GERMANY, GREECE, HUNGARY, IRELAND, ITALY, LATVIA,
			LITHUANIA, LUXEMBOURG, MALTA, NETHERLANDS, POLAND, PORTUGAL, ROMANIA, SLOVAKIA, SLOVENIA, SPAIN, SWEDEN,
			UNITED_KINGDOM);

	public static final List<Country> AFRICAN_COUNTRIES = ImmutableList.of(ALGERIA, ANGOLA, BENIN, BOTSWANA,
			BURKINA_FASO, BURUNDI, CAPE_VERDE, CAMEROON, CENTRAL_AFRICAN_REPUBLIC, CHAD, COMOROS, REPUBLIC_OF_THE_CONGO,
			DEMOCRATIC_REPUBLIC_OF_THE_CONGO, IVORY_COAST, DJIBOUTI, EGYPT, EQUATORIAL_GUINEA, ERITREA, ETHIOPIA, GABON,
			GAMBIA, GHANA, GUINEA, GUINEA_BISSAU, KENYA, LESOTHO, LIBERIA, LIBYA, MADAGASCAR, MALAWI, MALI, MAURITANIA,
			MAURITIUS, MOROCCO, MOZAMBIQUE, NAMIBIA, NIGER, NIGERIA, RWANDA, SAO_TOME_AND_PRINCIPE, SENEGAL, SEYCHELLES,
			SIERRA_LEONE, SOMALIA, SOUTH_AFRICA, SOUTH_SUDAN, SUDAN, SWAZILAND, TANZANIA, TOGO, TUNISIA, UGANDA, ZAMBIA,
			ZIMBABWE);

	public static final List<Country> ASIAN_COUNTRIES = ImmutableList.of(AFGHANISTAN, ARMENIA, AZERBAIJAN, BAHRAIN,
			BANGLADESH, BHUTAN, BRUNEI, CAMBODIA, CHINA, CYPRUS, GEORGIA, INDIA, INDONESIA, IRAN, IRAQ, ISRAEL, JAPAN,
			JORDAN, KAZAKHSTAN, KUWAIT, KYRGYZSTAN, LAOS, LEBANON, MALAYSIA, MALDIVES, MONGOLIA,
			REPUBLIC_OF_THE_UNION_OF_MYANMAR, NEPAL, NORTH_KOREA, OMAN, PAKISTAN, PHILIPPINES, QATAR, RUSSIA,
			SAUDI_ARABIA, SINGAPORE, SOUTH_KOREA, SRI_LANKA, SYRIA, TAIWAN, TAJIKISTAN, THAILAND, TIMOR_LESTE, TURKEY,
			TURKMENISTAN, UNITED_ARAB_EMIRATES, UZBEKISTAN, VIETNAM, YEMEN);

	public static final List<Country> NORTH_AMERICA_COUNTRIES = ImmutableList.of(ANTIGUA_AND_BARBUDA, BAHAMAS, BARBADOS,
			BELIZE, CANADA, COSTA_RICA, CUBA, DOMINICA, DOMINICAN_REPUBLIC, EL_SALVADOR, GRENADA, GUATEMALA, HAITI,
			HONDURAS, JAMAICA, MEXICO, NICARAGUA, PANAMA, SAINT_KITTS_AND_NEVIS, SAINT_LUCIA,
			SAINT_VINCENT_AND_THE_GRENADINES, TRINIDAD_AND_TOBAGO, UNITED_STATES);

	public static final List<Country> SOUTH_AMERICA_COUNTRIES = ImmutableList.of(ARGENTINA, BOLIVIA, BRAZIL, CHILE,
			COLOMBIA, ECUADOR, GUYANA, PARAGUAY, PERU, SURINAME, URUGUAY, VENEZUELA);

	public static final List<Country> AUSTRALIA_AND_OCEANIA_COUNTRIES = ImmutableList.of(AUSTRALIA, FIJI, KIRIBATI,
			MARSHALL_ISLANDS, MICRONESIA, NAURU, NEW_ZEALAND, PALAU, PAPUA_NEW_GUINEA, SAMOA, SOLOMON_ISLANDS, TONGA,
			TUVALU, VANUATU);

	private final String isoCode;

	private final String isoCode2;

	private final String name;

	private final String dialingCode;

	private final List<Language> languages;

	private final List<String> timezones;

	private Country(@Nonnull final String isoCode, @Nonnull final String isoCode2, @Nonnull final String name,
			@Nonnull final String dialingCode, @Nonnull final List<String> timezones,
			@Nonnull final Language... languages) {
		this.isoCode = isoCode;
		this.isoCode2 = isoCode2;
		this.name = name;
		this.dialingCode = dialingCode;
		this.timezones = timezones;
		this.languages = ImmutableList.copyOf(languages);
	}

	@Nonnull
	public String getIsoCode() {
		return this.isoCode;
	}

	@Nonnull
	public String getIsoCode2() {
		return this.isoCode2;
	}

	@Nonnull
	public String getName() {
		return this.name;
	}

	@Nonnull
	public String getDialingCode() {
		return this.dialingCode;
	}

	@Nonnull
	public List<String> getTimezones() {
		return timezones;
	}

	/**
	 * Unmodifieble view of this countries spoken languages.
	 * <p>
	 * 
	 * @return
	 */
	@Nonnull
	public List<Language> getLanguages() {
		return languages;
	}

	public boolean isEuropeanCountry() {
		return EUROPEAN_COUNTRIES.contains(this);
	}

	public boolean isAfricanCountry() {
		return AFRICAN_COUNTRIES.contains(this);
	}

	public boolean isEuropeanUnionMember() {
		return EUROPEAN_UNION_COUNTRIES.contains(this);
	}

	@Nonnull
	public Optional<Continent> getContinent() {
		final Continent continent = EUROPEAN_COUNTRIES.contains(this) ? Continent.EUROPE
				: AFRICAN_COUNTRIES.contains(this) ? Continent.AFRICA
						: ASIAN_COUNTRIES.contains(this) ? Continent.ASIA
								: NORTH_AMERICA_COUNTRIES.contains(this) ? Continent.NORTH_AMERICA
										: SOUTH_AMERICA_COUNTRIES.contains(this) ? Continent.SOUTH_AMERICA
												: AUSTRALIA_AND_OCEANIA_COUNTRIES.contains(this) ? Continent.AUSTRALIA
														: null;
		return Optional.ofNullable(continent);
	}

	@Nonnull
	public static Country fromIsoCode(@Nonnull final String isoCode) {
		checkArgument(!Strings.isNullOrEmpty(isoCode), "The isocode must not be null nor empty");

		for (final Country c : values()) {
			if (c.isoCode.equalsIgnoreCase(isoCode)) {
				return c;
			}
		}
		throw new IllegalArgumentException("Unsupported country iso-code: " + isoCode);
	}

	@Nonnull
	public static Country fromIsoCode2(@Nonnull final String isoCode2) {
		checkArgument(!Strings.isNullOrEmpty(isoCode2), "The isocode must not be null nor empty");

		for (final Country c : values()) {
			if (c.isoCode2.equalsIgnoreCase(isoCode2)) {
				return c;
			}
		}
		throw new IllegalArgumentException("Unsupported country iso-code2: " + isoCode2);
	}

	/**
	 * Returns sorted sorted, cannot be modified.
	 * <p>
	 * 
	 * @return
	 */
	public static Set<Country> countries() {
		@SuppressWarnings("SetReplaceableByEnumSet")
		SortedSet<Country> countries = new TreeSet<>(new Comparator<Country>() {

			@Override
			public int compare(Country o1, Country o2) {
				return o1.name().compareToIgnoreCase(o2.name());
			}

		});
		countries.addAll(Arrays.asList(values()));
		return Collections.unmodifiableSortedSet(countries);
	}

	@Nonnull
	private static List<String> timezones(final String... timezones) {
		final List<String> timezoneList = ImmutableList.copyOf(timezones);
		return timezoneList.stream().map(String::trim).collect(Collectors.toList());
	}

	@Override
	public String toString() {
		return name;
	}

}
