/**
 * Basic pojos for use in other projects.
 */
@XmlSchema(namespace = "urn://common.anosym.com/basic")
package anosym.common;

import jakarta.xml.bind.annotation.XmlSchema;
