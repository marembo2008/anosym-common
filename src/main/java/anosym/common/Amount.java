package anosym.common;

import com.google.common.annotations.VisibleForTesting;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;
import javax.annotation.Nonnull;

import static anosym.common.Amount.CentRoundingMode.SPECIFIED_ACCURACY;
import static anosym.common.util.StringsUtil.group;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.lang.Integer.max;
import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.round;
import static java.lang.String.format;

/**
 * Maintains the amount to the nearest hundred cents.
 *
 * @author marembo
 */
public final class Amount implements Comparable<Amount>, Serializable {

    private static final long serialVersionUID = 12282948294L;

    //BigDecimal.doubleValue() strips off all zeros at the end, so we cannot format correctly.
    private static final double MANY_DECIMALS = 1.0E-15;

    @VisibleForTesting
    static final String GLOBAL_CENT_ROUNDING_MODE_PROPERTY = "anosym.amount.centRoundingMode";

    @VisibleForTesting
    static final String GLOBAL_ACCURACY_SCALE = "anosym.amount.accuracyScale";

    @Nonnull
    private final Currency currency;

    @Nonnull
    private final long valueInCents;

    private final CentRoundingMode centRoundingMode;

    private final int accuracyScale;

    public Amount(@Nonnull final Currency currency, @Nonnull final int valueInCents) {
        this(currency, valueInCents, defaultCentRoundingMode(), defaultAccuracyScale());
    }

    public Amount(@Nonnull final Currency currency, @Nonnull final long valueInCents) {
        this(currency, valueInCents, defaultCentRoundingMode(), defaultAccuracyScale());
    }

    public Amount(@Nonnull final Currency currency,
                  @Nonnull final long valueInCents,
                  @Nonnull final CentRoundingMode centRoundingMode) {
        this(currency, valueInCents, centRoundingMode, defaultAccuracyScale());
    }

    public Amount(@Nonnull final Currency currency,
                  @Nonnull final long valueInCents,
                  @Nonnull final CentRoundingMode centRoundingMode,
                  @Nonnull final int accuracyScale) {
        this.currency = checkNotNull(currency, "The currency must not be null");
        this.centRoundingMode = checkNotNull(centRoundingMode, "The centRoundingMode must not be null");
        this.valueInCents = normalize(checkNotNull(valueInCents, "The value must not be null"));
        this.accuracyScale = accuracyScale;
        checkAccuracyScaleAndMode();
    }

    public Amount(@Nonnull final Currency currency, @Nonnull final BigDecimal valueWithDecimalCents) {
        this(currency,
             valueWithDecimalCents.setScale(max(valueWithDecimalCents.scale(), 2)).doubleValue(),
             SPECIFIED_ACCURACY,
             max(valueWithDecimalCents.scale(), 2));
    }

    public Amount(@Nonnull final Currency currency, @Nonnull final Double valueWithDecimalCents) {
        this(currency, valueWithDecimalCents, defaultCentRoundingMode(), defaultAccuracyScale());
    }

    public Amount(@Nonnull final Currency currency,
                  @Nonnull final Double valueWithDecimalCents,
                  @Nonnull final CentRoundingMode centRoundingMode) {
        this(currency, valueWithDecimalCents, centRoundingMode, defaultAccuracyScale());
    }

    public Amount(@Nonnull final Currency currency,
                  @Nonnull final Double valueWithDecimalCents,
                  @Nonnull final CentRoundingMode centRoundingMode,
                  @Nonnull final int accuracyScale) {
        this.currency = checkNotNull(currency, "The currency must not be null");
        this.centRoundingMode = checkNotNull(centRoundingMode, "The centRoundingMode must not be null");
        this.accuracyScale = checkNotNull(accuracyScale, "The accuracyScale must not be null");
        checkAccuracyScaleAndMode();

        final double scale = pow(10, accuracyScale);
        final long truncatedCents
                = round(checkNotNull(valueWithDecimalCents, "The valueWithDecimalCents must not be null") * scale);
        this.valueInCents = normalize(truncatedCents);

    }

    public Amount(@Nonnull final Currency currency,
                  @Nonnull final Number valueWithDecimalCents,
                  @Nonnull final CentRoundingMode centRoundingMode,
                  @Nonnull final int accuracyScale) {
        this(currency, checkNotNull(valueWithDecimalCents, "The amount value must be specified").doubleValue(),
             centRoundingMode, accuracyScale);

    }

    private void checkAccuracyScaleAndMode() {
        //It does not matter if the specified scale is one.
        if (centRoundingMode == SPECIFIED_ACCURACY) {
            return;
        }

        //Otherwise, the centrounding modes require a scale of 2.
        checkState(accuracyScale == 2,
                   "The accuracy scale and cent roundingmode are invalid: {%s != %s}",
                   centRoundingMode,
                   accuracyScale);
    }

    private long normalize(@Nonnull final long valueInCents) {
        //Only if the centRoundingMode=NEAREST_TEN_CENTS;
        switch (centRoundingMode) {
            case NEAREST_HUNDRED_CENTS: {
                final long cents = valueInCents % 100;
                final long roundedCents = cents < 50 ? 0 : 100;
                return (valueInCents / 100) * 100 + roundedCents;
            }
            case NEAREST_FIFTY_CENTS: {
                final long cents = valueInCents % 100;
                final long roundedCents = cents > 0 ? (cents < 50 ? 50 : 100) : 0;
                return (valueInCents / 100) * 100 + roundedCents;
            }
            case NEAREST_TEN_CENTS: {
                final long cents = valueInCents % 100;
                final long wholeAmount = valueInCents / 100;
                final long lastCentDigit = cents % 10;
                final long roundedLastCentDigit = lastCentDigit < 5 ? 0 : 10;
                final long roundedCents = (cents / 10) * 10 + roundedLastCentDigit;
                return wholeAmount * 100 + roundedCents;
            }
            case NEAREST_FIVE_CENTS: {
                final long cents = valueInCents % 100;
                final long wholeAmount = valueInCents / 100;
                final long lastCentDigit = cents % 10;
                final long roundedLastCentDigit = lastCentDigit > 0 ? (lastCentDigit <= 5 ? 5 : 10) : 0;
                final long roundedCents = (cents / 10) * 10 + roundedLastCentDigit;
                return wholeAmount * 100 + roundedCents;
            }
            default:
                return valueInCents;
        }
    }

    /**
     * We cant make it a static constant as it may change during the execution of the program, or be set late after the
     * Amount.class has been initialized.
     */
    private static CentRoundingMode defaultCentRoundingMode() {
        return CentRoundingMode.valueOf(System.getProperty(GLOBAL_CENT_ROUNDING_MODE_PROPERTY, "NEAREST_CENT").
                toUpperCase());
    }

    private static int defaultAccuracyScale() {
        return Integer.parseInt(System.getProperty(GLOBAL_ACCURACY_SCALE, "2"));
    }

    @Nonnull
    public Currency getCurrency() {
        return currency;
    }

    @Nonnull
    public long getValueInCents() {
        return valueInCents;
    }

    @Nonnull
    public long getLongValue() {
        return getValue().longValue();
    }

    @Nonnull
    public int getIntValue() {
        return getValue().intValue();
    }

    @Nonnull
    public CentRoundingMode getCentRoundingMode() {
        return centRoundingMode;
    }

    @Nonnull
    public int getAccuracyScale() {
        return accuracyScale;
    }

    @Nonnull
    public BigDecimal getValue() {
        return new BigDecimal((double) valueInCents)
                .divide(new BigDecimal(10).pow(accuracyScale), accuracyScale, RoundingMode.UP);
    }

    @Nonnull
    public final Amount add(@Nonnull final Amount amountToAdd) {
        checkNotNull(amountToAdd, "The amount to add must be specified");
        checkState(this.currency == amountToAdd.currency, "Cannot add amounts with differing currency");

        //We need to transform the values in cents to same accuracy.
        final int thisAccuracyScale = this.accuracyScale;
        final int thatAccuracyScale = amountToAdd.accuracyScale;
        final int commonAccuracyScale = max(thisAccuracyScale, thatAccuracyScale);
        final CentRoundingMode commonCentRoundingMode = amountToAdd.centRoundingMode != centRoundingMode
                ? SPECIFIED_ACCURACY : centRoundingMode;
        final Amount thisAmount = extrapolate(this, commonAccuracyScale);
        final Amount thatAmount = extrapolate(amountToAdd, commonAccuracyScale);
        return new Amount(thisAmount.currency,
                          thisAmount.valueInCents + thatAmount.valueInCents,
                          commonCentRoundingMode,
                          commonAccuracyScale);
    }

    @Nonnull
    public final Amount subtract(@Nonnull final Amount amountToSubtract) {
        return add(amountToSubtract.negate());
    }

    @Nonnull
    public final Amount negate() {
        return new Amount(currency, -valueInCents, centRoundingMode, accuracyScale);
    }

    @Nonnull
    public final Amount multiply(final int multiplicant) {

        return new Amount(this.currency, this.valueInCents * multiplicant, centRoundingMode, accuracyScale);
    }

    @Nonnull
    public final Amount multiply(@Nonnull final Number multiplicant) {

        final long valueInCents_ = (long) round((this.valueInCents * multiplicant.doubleValue()));
        return new Amount(this.currency, valueInCents_, centRoundingMode, accuracyScale);
    }

    @Nonnull
    public final Amount divide(final int dividor) {

        return new Amount(this.currency, this.valueInCents / dividor, centRoundingMode, accuracyScale);
    }

    @Nonnull
    public final Amount copy() {
        return new Amount(this.currency, this.valueInCents, centRoundingMode, accuracyScale);
    }

    @Nonnull
    public final Amount scale(final int newScale) {
        final int scaling = newScale - this.accuracyScale;
        final double multiply = pow(10, scaling);
        final long newValueInCents = (long) (multiply * valueInCents);
        return new Amount(currency, newValueInCents, centRoundingMode, newScale);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currency, valueInCents, centRoundingMode, accuracyScale);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Amount other = (Amount) obj;
        if (this.currency != other.currency) {
            return false;
        }

        final int thisAccuracyScale = this.accuracyScale;
        final int thatAccuracyScale = other.accuracyScale;
        final int commonAccuracyScale = max(thisAccuracyScale, thatAccuracyScale);
        final Amount thisAmount = extrapolate(this, commonAccuracyScale);
        final Amount thatAmount = extrapolate(other, commonAccuracyScale);
        return thisAmount.valueInCents == thatAmount.valueInCents
                && thisAmount.centRoundingMode == thatAmount.centRoundingMode;
    }

    private String getToString() {
        final long scale = (long) pow(10, accuracyScale);
        final long wholeAmount = valueInCents / scale;
        final long cents = abs(valueInCents % scale);
        return format("%s.%0" + accuracyScale + "d", group(String.valueOf(wholeAmount), ",", 3), cents);
    }

    @Override
    public String toString() {
        return format("%s %s", this.currency.name(), getToString());
    }

    @Override
    public int compareTo(final Amount amount) {
        checkState(this.currency == amount.currency, "Cannot compare two amounts with different currencies");

        final int thisAccuracyScale = this.accuracyScale;
        final int thatAccuracyScale = amount.accuracyScale;
        final int commonAccuracyScale = max(thisAccuracyScale, thatAccuracyScale);
        final Amount thisAmount = extrapolate(this, commonAccuracyScale);
        final Amount thatAmount = extrapolate(amount, commonAccuracyScale);
        return Long.valueOf(thisAmount.valueInCents).compareTo(thatAmount.valueInCents);
    }

    public static long convertToCents(@Nonnull final Number amountsWithDecimalCents) {
        return new Amount(Currency.AED, amountsWithDecimalCents.doubleValue()).getValueInCents();
    }

    @Nonnull
    private Amount extrapolate(@Nonnull final Amount amount, @Nonnull final int newAccuracyScale) {
        final double multiplier = pow(10, newAccuracyScale - amount.accuracyScale);
        final long newValueInCents = round(multiplier * amount.valueInCents);
        return new Amount(amount.currency, newValueInCents, SPECIFIED_ACCURACY, newAccuracyScale);
    }

    public static enum CentRoundingMode {

        SPECIFIED_ACCURACY, //Implies no rounding mode is done.
        NEAREST_CENT,
        NEAREST_FIVE_CENTS,
        NEAREST_TEN_CENTS,
        NEAREST_FIFTY_CENTS,
        NEAREST_HUNDRED_CENTS;
    }
}
